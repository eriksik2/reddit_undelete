
var info_text = $("#info_text");
var checkbox_author = $("#show_author_deleted");
var checkbox_mod = $("#show_mod_deleted");
var settings;

function saveChanges(){
    chrome.storage.sync.set(settings, () => {
        info_text.text("changes saved.");
    });
}

chrome.storage.sync.get({
    show_author_deleted: true,
    show_mod_deleted: true
}, items => {
    settings = Object.assign({}, items);
    checkbox_author.prop("checked", settings.show_author_deleted);
    checkbox_mod.prop("checked", settings.show_mod_deleted);
    
    checkbox_author.click(e => {
        settings.show_author_deleted = checkbox_author.prop("checked");
        saveChanges();
    });
    checkbox_mod.click(e => {
        settings.show_mod_deleted = checkbox_mod.prop("checked");
        saveChanges();
    });
});