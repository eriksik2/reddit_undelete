#!/bin/bash

# files need to initially compile before starting watchify
if [ ! -e build/script.js ]; then
    ./node_modules/.bin/tsc --skipLibCheck
fi

# start tsc and watchify
./node_modules/.bin/tsc --watch --skipLibCheck &
./node_modules/.bin/watchify build/script.js -v -o output/script.js