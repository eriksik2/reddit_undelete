

export class Settings {
    show_author_deleted = true;
    show_mod_deleted = true;
}

const private_settings = new Settings();

// initially get all settings
chrome.storage.sync.get(items => {
    Object.assign(private_settings, items);
});

// listen for setting changes
chrome.storage.onChanged.addListener((changes, area) => {
    if(area != "sync") return;
    chrome.storage.sync.get(Object.keys(changes), items => {
        Object.assign(private_settings, items);
    });
});

// return a copy of private_settings so it doesnt change independently of stored settings
export function get(){
    return Object.assign({}, private_settings);
}