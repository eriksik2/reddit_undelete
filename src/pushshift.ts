
import * as types from "./types";
import * as util from "./util";

function getCommentDataInternal(id_array: string[]): Promise<types.CommentData[]> {
    if(id_array.length <= 0) return Promise.resolve([]);
    const fields: (keyof types.CommentData)[] = ['body', 'id', 'parent_id', 'author', 'created_utc', 'score'];
    const url = "https://api.pushshift.io/reddit/comment/search?fields="+fields.join(",")+"&ids="+id_array.join(",");

    return window.fetch(url)
        .then(json => json.json())
        .then(data => data.data)
        .then((data: types.CommentData[]) => {
            // TODO markdown
            data.forEach(comment => {
                comment.id = "t1_"+comment.id;
            });
            return data;
        });
}

// get comment data from an array of comment ids
export function getCommentData(id_array: string[]): Promise<types.CommentData[]> {
    if (id_array.length <= 0) return Promise.resolve([]);
    
    return Promise.all(util.chunkedCall(id_array, 350, getCommentDataInternal))
        .then(util.combine);
}

// get ids of all comments in a thread
export function getCommentIds(thread_id: string): Promise<string[]> {
    const url = "https://api.pushshift.io/reddit/submission/comment_ids/" + thread_id;

    return window.fetch(url)
        .then(json => json.json())
        .then(data => data.data)
        .then((data: string[]) => {
            return data.map(id => "t1_"+id);
        });
}