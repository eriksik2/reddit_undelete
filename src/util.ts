
import * as types from "./types";

// same as Object.entries
export function entries(obj: {}) {
    return Object.keys(obj).map(key => ([key, (obj as any)[key]])) as [string, any][];
}

// combine array of arrays into a single array
export function combine<T>(array: T[][]): T[] {
    return array.reduce((acc, value) => acc.concat(value));
}

// split an array into multiple arrays that arent larger than chunk_size
export function chunk<T>(array: T[], chunk_size: number): T[][] {
    const chunks: T[][] = [];
    for(var i = 0; i < array.length; i += chunk_size)
        chunks.push(array.slice(i, i + chunk_size));
    return chunks;
}

// call a function multiple times on array split into smaller arrays no larger than chunk_size
export function chunkedCall<T, R>(array: T[], chunk_size: number, callback: (v: T[]) => R): R[] {
    return chunk(array, chunk_size).map(value => callback(value));
}

// returns true if comment has been removed by a moderator
export function commentRemovedByMod(comment: types.CommentData){
    return comment.body == "[removed]";
}

// returns true if comment has been deleted by its author
export function commentRemovedByUser(comment: types.CommentData){
    return comment.body == "[deleted]";
}

// returns true if comment has been deleted by its author or removed by a moderator
export function commentRemoved(comment: types.CommentData){
    return commentRemovedByMod(comment) || commentRemovedByUser(comment);
}

export function commentRemovedBy(comment: types.CommentData) {
    switch(comment.body){
        case "[removed]": return types.RemovedBy.MOD;
        case "[deleted]": return types.RemovedBy.USER;
        default: return types.RemovedBy.NONE;
    }
}