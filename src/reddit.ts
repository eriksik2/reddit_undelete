
import * as types from "./types";
import * as util from "./util";

function getCommentDataInternal(id_array: string[]): Promise<types.CommentData[]> {
    if(id_array.length <= 0) return Promise.resolve([]);
    const url = "https://api.reddit.com/api/info?id="+id_array.join(",");

    return window.fetch(url, { credentials: "omit" }) // omit credentials so that reddits api doesnt return localized [removed] and [delete]
        .then(json => json.json())
        .then(data => data.data.children.map((data:any) => data.data))
        .then((data: types.CommentData[]) => {

            data.forEach(comment => {
                comment.id = "t1_"+comment.id;
            });
            return data;
        });
}

// get comment data from an array of comment ids
export function getCommentData(id_array: string[]): Promise<types.CommentData[]> {
    if(id_array.length <= 0) return Promise.resolve([]);
    
    return Promise.all(util.chunkedCall(id_array, 100, getCommentDataInternal))
        .then(util.combine);
}