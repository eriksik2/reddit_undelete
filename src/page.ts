
import * as $ from "jquery";

import * as util from "./util";
import * as types from "./types";

export enum PageType { THREAD, COMMENT }

export interface PageData {
    threadId: string;
    pageType: PageType;
}

// get page data, returns null if not on a thread page
export function getPageData(): PageData | null {
    var threadId: string;
    var pageType: PageType;
    const path = window.location.pathname;
    const match = path.match(/comments[\\/]([a-zA-Z0-9]+)[\\/][a-z_]+([\\/][a-zA-Z0-9]+)?/);
    if(!match || match.length <= 1) return null; // TODO : better error maybe
    threadId = "t3_" + match[1];
    if(match.length >= 2) pageType = PageType.COMMENT;
    else pageType = PageType.THREAD;
    return {
        threadId,
        pageType
    };
}

// get element that contains children of page element that represents id
export function getElementChildrenNode(id: string, in_node: JQuery<HTMLElement> = $(document.body)): JQuery<HTMLElement> | null {
    if(id.startsWith("t3_")){
        var e = in_node.find("#siteTable_" + id);
        if(!e.length) e = in_node.filter("#siteTable_" + id);
        if(!e.length) return null;
        return e;
    }
    var e = in_node.find(".comment#thing_" + id + " > .child > #siteTable_" + id);
    if (!e.length) e = in_node.filter(".comment#thing_" + id + " > .child > #siteTable_" + id);
    if (!e.length){
        e = in_node.filter(".comment#thing_" + id + " > .child");
        if (!e.length) return null;
        const st = newNode("div", "sitetable listing", "siteTable_" + id);
        e.append(st);
        return st;
    }
    return e;
}

// get page element that represents id
export function getElement(id: string, in_node: JQuery<HTMLElement> = $(document.body)): JQuery<HTMLElement> | null {
    // comment and thread elements have diffrently formed ids
    const query = id.startsWith("t3_") ? "#siteTable_"+id : "#thing_"+id;

    var element = in_node.find(query);
    if(!element.length) element = in_node.filter(query);
    if(!element.length) return null;
    return element;
}

export function getMoreChildrenButtons(in_node: JQuery<HTMLElement> = $(document.body)){
    return in_node.find("span.morecomments > a.button")
        .add(in_node.filter("span.morecomments > a.button"));
}

// local helper for converting an object to a string that jquery can parse as css (jquery already does this but the native way doesnt work with !important)
function cssText(css: {}) {
    var str = "";
    for (const [key, value] of util.entries(css)) str += key + ":" + value + ";";
    return str;
}

// local helper for creating new elements quickly
function newNode(type: (keyof HTMLElementTagNameMap), classname?: string, id?: string){
    const element = $(document.createElement(type));
    if(classname) element.attr("class", classname);
    if(id) element.attr("id", id);
    return element;
}

// constructs a comment element from CommentData
export function newComment(data: types.RemovedComment, depth = 0): JQuery<HTMLElement> {

    var bottom_text: string;
    switch(data.remover){
        case types.RemovedBy.MOD: bottom_text = "[Removed by moderator]"; break;
        case types.RemovedBy.USER: bottom_text = "[Deleted by author]"; break;
        default: bottom_text = "[Comment not removed/deleted]";
    }

    var author; 
    if(data.author == "[deleted]")
        author = newNode("p", "author") .append(data.author);
    else author = newNode("a", "author").append(data.author)
        .attr("href", "https://www.reddit.com/user/" + data.author);

    const sitetable = newNode("div", "sitetable listing", "siteTable_" + data.id);
    // recursively build comment tree
    for(const child of data.children)
        sitetable.append(newComment(child, depth+1));

    var main_css: any = {
        "background-color": depth%2==0 ? 
            "rgba(255, 230, 230, 1) !important" :
            "rgba(248, 200, 200, 1) !important"
    };
    if (data.remover == types.RemovedBy.NONE) main_css = {};

    const date = new Date(data.created_utc*1000);
    const div = newNode("div", "reddit-undelete thing comment noncollapsed id-"+data.id, "thing_"+data.id)
        .css("cssText", cssText(main_css))
        .append(newNode("div", "midcol")
            .append(newNode("div", "arrow up"))
        )
        .append(newNode("div", "entry reddit-undelete")
            .append(newNode("div", "tagline")
                .append(newNode("a", "expand")
                    .attr("href", "javascript:void(0)")
                    .attr("onclick", "return togglecomment(this)")
                    .append("[-]")
                )
                .append(author)
                .append(newNode("span", "score unvoted")
                    .attr("title", data.score)
                    .append(" "+data.score+" Points ")
                )
                .append(newNode("time", "live-timestamp")
                    .append(date.toLocaleString())
                    .attr("datetime", date.toISOString())
                )
            )
            .append(newNode("div", "usertext")
                .append(newNode("div", "md")
                    .append(data.body)
                )
            )
            .append(newNode("ul", "flat-list buttons")
                .append(newNode("p")
                    .append(bottom_text)
                )
            )
        )
        .append(newNode("div", "child")
            .append(sitetable)
        );
    return div;
}