
import * as util from "./util";

export interface CommentData {
    id: string;
    body: string;
    parent_id: string;
    author: string;
    created_utc: number;
    score: number;
};

export enum RemovedBy { MOD, USER, NONE };

export class RemovedComment implements CommentData {
    static too_quick_msg = "[Comment was removed too quickly to be archived]";
    id: string;
    body: string;
    parent_id: string;
    author: string;
    created_utc: number;
    score: number;
    children: RemovedComment[] = [];

    constructor(
        comment: CommentData,
        public remover: RemovedBy
    ){

        if (util.commentRemoved(comment)) 
            this.body = RemovedComment.too_quick_msg;
        else this.body = comment.body;

        this.id = comment.id;
        this.parent_id = comment.parent_id;
        this.author = comment.author;
        this.created_utc = comment.created_utc;
        this.score = comment.score;
    }

    // find comment with id in this comment tree
    getComment(id: string){
        if(this.id == id) return this;
        for(const child of this.children)
            if (child) return child;
        return null;
    }

    removedTooQuick() {
        return this.body == RemovedComment.too_quick_msg;
    }
}