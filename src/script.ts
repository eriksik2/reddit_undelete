
// TODO : dont show level 0 deleted comments in comment permalink page (use page.PageData)
// TODO : look into performance measuring of chrome extensions
// TODO : popup with settings:
//          style control
//          show deleted comments(on/off)
// TODO : parse markdown in comment bodies
// TODO : conform to reddit api rules (https://github.com/reddit-archive/reddit/wiki/API)
// TODO : make deleted comments and their children appear in the correct order based on users reddit sort mode
// TODO : add permalink button to deleted comments

// TODO / BUG : https://www.reddit.com/r/nanotrade/comments/7vwl86/what_are_the_faults_of_nano/dtvusnf/
//            : deleted comments gets restored but its children dont get moved over

// TODO / BUG : https://www.reddit.com/r/nanotrade/comments/7vwl86/what_are_the_faults_of_nano/dtwxpc8/
//            : comment gets completely removed from its permalink (maybe because its a direct child of a deleted comment?)

import * as $ from "jquery";

import * as util from "./util";
import * as reddit from "./reddit";
import * as archive from "./pushshift";
import * as page from "./page";
import * as types from "./types";
import * as settings from "./settings";


var orphan_buffer: types.CommentData[] = [];
var found_orphans: JQuery<HTMLElement> = $([]);
var comment_buffer: types.RemovedComment[] = [];


// if any of the comments in comment_buffer has its parent on the page they get removed from the buffer and added to the page
function checkCommentBuffer(in_node: JQuery<HTMLElement> = $(document.body)){
    const found_parent: types.RemovedComment[] = [];
    for (const data of comment_buffer){
        var parent = page.getElementChildrenNode(data.parent_id, in_node);
        // if parent doesnt exist in the dom, check if parent exists as an orphan
        if (!parent){
            parent = page.getElementChildrenNode(data.parent_id, found_orphans);
            //parent = found_orphans.filter("#thing_" + data.parent_id);
            if(!parent) continue; // otherwise skip this comment for now
        }

        // parent has unloaded children, skip this element (because removed comments should be shown last)
        if (parent.children(".morechildren").length) continue;

        // get orphan children that belong to this comment
        const children = found_orphans.filter("[undelete-orphan-parent=" + data.id + "]");
        found_orphans = found_orphans.not(children);

        const new_c = page.newComment(data);
        new_c.find("#siteTable_"+data.id).prepend(children);// prepend orphan children so that they appear before any deleted child
        parent.append(new_c);
        found_parent.push(data);
    }
    // remove comments that found its parent from buffer
    comment_buffer = comment_buffer.filter(comment => found_parent.indexOf(comment) == -1);
}

// remove all comments with the .deleted class
function removeDeletedComments(in_node: JQuery<HTMLElement> = $(document.body)) {
    in_node.find(".comment.deleted")
        .add(in_node.filter(".comment.deleted")).remove();
}

// finds and collects orphan comments from the dom into found_orphans
function findOrphans(in_node: JQuery<HTMLElement> = $(document.body)){
    const found: types.CommentData[] = [];
    for(const o of orphan_buffer){
        const node = in_node.find("#thing_" + o.id).add(in_node.filter("#thing_" + o.id));
        if(!node.length) continue;
        node.attr("undelete-orphan-parent", o.parent_id);
        found_orphans = found_orphans.add(node.detach());
        found.push(o);
    }
    orphan_buffer = orphan_buffer.filter(c => found.indexOf(c) == -1);
}

// common process that any new comments on the page need to go through
function processComments(in_node: JQuery<HTMLElement> = $(document.body)){
    findOrphans(in_node);
    removeDeletedComments(in_node);
}

function morechildren_onclick(e: JQuery.Event<HTMLElement, null>)  {
    const button = $(e.delegateTarget).parent().parent().parent();
    const root = button.parent();
    const old_children = root.children();

    // TODO use something better than just a delay (if there is a better solution)
    const callback = () => {
        // if morechildren button still exists, wait some more
        if (root.find(button).length) {
            setTimeout(callback, 1000);
            return;
        }
        const new_children = root.children(":not(.clearleft)").not(old_children);

        // add this callback to any newfound morechildren buttons
        page.getMoreChildrenButtons(new_children)
            .click(morechildren_onclick);

        processComments(new_children);
        checkCommentBuffer(root);
    };
    setTimeout(callback, 1000);
}

const pageData = page.getPageData();
if(pageData){

    page.getMoreChildrenButtons().click(morechildren_onclick); // register callback for morechildren buttons that are on the page initially

    // get array of all comment_ids in this thread
    archive.getCommentIds(pageData.threadId).then(comment_ids => {
        // get commentdata from reddit so we can compare it with commentdata from pushshift
        reddit.getCommentData(comment_ids).then(reddit_data => {
            const removed_ids: string[] = [];
            for (const comment of reddit_data) {
                if (util.commentRemoved(comment)) removed_ids.push(comment.id);
            }
            // get archived data on removed comments from pushshift 
            archive.getCommentData(removed_ids)
                .then(archive_data => archive_data.map(data => {
                    // get remover from reddit data because archive data wont be removed/deleted
                    const reddit = reddit_data.find(comment => comment.id == data.id) as types.CommentData;
                    data.score = reddit.score; // reddit has more up to date scores
                    return new types.RemovedComment(data, util.commentRemovedBy(reddit));
                }))
                .then(mapped_data => {
                    const setting = settings.get();
                    for (const removed_comment of mapped_data) {
                        // if comment was removed before being archived, ignore it
                        if (removed_comment.removedTooQuick()) continue;

                        // skip based on user settings - show user/mod deleted comments
                        if (removed_comment.remover == types.RemovedBy.MOD && !setting.show_mod_deleted) continue;
                        if (removed_comment.remover == types.RemovedBy.USER && !setting.show_author_deleted) continue;

                        // build comment trees of deleted comments
                        // if parent is a removed comment, add this comment to its children
                        if (removed_comment.parent_id.startsWith("t1_")) {
                            const comment = mapped_data.find(c => c.id == removed_comment.parent_id);
                            if (comment) {
                                comment.children.push(removed_comment);
                                continue; // dont want removed_comment in comment_buffer
                            }
                        }
                        comment_buffer.push(removed_comment);
                    }

                    // find comments whose parent is a deleted comment, put them in orphan_buffer
                    for (const reddit of reddit_data) {
                        if (reddit.parent_id.startsWith("t3")) continue; // id that starts with t3 will always be the thread, dont need to check these
                        if (mapped_data.find(c => c.id == reddit.parent_id)){
                            orphan_buffer.push(reddit);
                        }
                    }

                    processComments();
                    checkCommentBuffer();
                });
        });
    });

}
